<?php
namespace App;

class Helper
{
    public static function formatDateFromDb($data){
	    if(!empty($data)){
	        return date("d/m/Y", strtotime($data));
	    } else{
	        return $data;
	    }
    }

    public static function formatDateToDb($data){
	    if(!empty($data)){
	    	$partes = explode('/', $data);
	        return $partes[2].'-'.$partes[1].'-'.$partes[0];
	    } else{
	        return $data;
	    }
    }
}
