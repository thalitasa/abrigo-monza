<?php
declare(strict_types=1);

use Slim\App;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Interfaces\RouteCollectorProxyInterface as Group;

use App\Helper;

return function (App $app) {
    $container = $app->getContainer();

    // --------------------------------------------------
    // COLOCAR AQUI A CONFIGURAÇÃO DE CONEXÃO COM O BANCO
    $db['host']   = 'localhost:3306';
	$db['dbname'] = 'abrigo-monza';
	$db['user']   = 'root';
	$db['pass']   = '';
    // --------------------------------------------------

	// Conecta ao banco de dados
	$pdo = new \PDO('mysql:host=' . $db['host'] . ';dbname=' . $db['dbname'], $db['user'], $db['pass']);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);


    // ----------------------
    // 		INÍCIO ROTAS

    $app->group('/animais', function (Group $group) use ($container, $pdo) {

    	// Listar
    	$group->get('', function (Request $request, Response $response) use ($pdo) {

    		$sql = "SELECT a.id, a.nome, a.raca, a.raca_id, a.sexo, a.especie, a.data_nascimento, a.data_atualizacao, a.data_falecimento, a.idade, a.faleceu FROM vw_animais a";
			$stmt = $pdo->prepare($sql);
			$stmt->execute();
			$results = $stmt->fetchAll();

			$animais = [];
			foreach ($results as $key => $value) {
                $value['data_nascimento'] = Helper::formatDateFromDb($value['data_nascimento']);
                $value['data_atualizacao'] = Helper::formatDateFromDb($value['data_atualizacao']);
                $value['data_falecimento'] = Helper::formatDateFromDb($value['data_falecimento']);

                $animais[] = $value;
			}

			$animais = json_encode($animais);

			$response->getBody()->write($animais);
			return $response;
	    });

        // GET animal
        $group->get('/{id}', function (Request $request, Response $response) use ($pdo) {

            $id = $request->getAttribute('route')->getArgument('id');

            $sql = "SELECT a.id, a.nome, a.raca, a.raca_id, a.sexo, a.especie, a.data_nascimento, a.data_atualizacao, a.data_falecimento, a.idade, a.faleceu FROM vw_animais a WHERE a.ID = " . $id;
            $stmt = $pdo->prepare($sql);
            $stmt->execute();
            $results = $stmt->fetchAll();

            if(isset($results[0]))
                $animal = $results[0];
            else
                $animal = new stdClass;

            $response->getBody()->write(json_encode($animal));
            return $response;
        });

        // Salvar (insert/update)
        $group->post('/salvar', function (Request $request, Response $response) use ($pdo) {

            $data = $request->getBody()->getContents();

            $animal = json_decode($data, true);
            $animal['data_atualizacao'] = date('Y-m-d');
            $animal['data_falecimento'] = (!isset($animal['data_falecimento']) || $animal['data_falecimento'] == '') ? null : $animal['data_falecimento'];

            $animal['data_nascimento']  = Helper::formatDateToDb($animal['data_nascimento']);
            $animal['data_falecimento'] = Helper::formatDateToDb($animal['data_falecimento']);

            // Novo
            if($animal['id'] == 0){
                try{
                    $sql = "CALL animais_insert (?, ?, ?, ?, ?, ?, ?, @status, @mensagem)";
                    $stmt = $pdo->prepare($sql);

                    $stmt->bindParam(1, $animal['nome']);
                    $stmt->bindParam(2, $animal['raca_id']);
                    $stmt->bindParam(3, $animal['sexo']);
                    $stmt->bindParam(4, $animal['especie']);
                    $stmt->bindParam(5, $animal['data_nascimento']);
                    $stmt->bindParam(6, $animal['data_atualizacao']);
                    $stmt->bindParam(7, $animal['data_falecimento']);

                    $stmt->execute();

                    $sucesso = true;
                    $mensagem = 'Animal gravado com sucesso';
                } catch(Exception $e){
                    $sucesso = false;
                    $mensagem = 'Erro ao gravar animal';
                }
            }
            // Editar
            else{
                try{
                    $sql = "CALL animais_update (?, ?, ?, ?, ?, ?, ?, ?)";
                    $stmt = $pdo->prepare($sql);

                    $stmt->bindParam(1, $animal['id']);
                    $stmt->bindParam(2, $animal['nome']);
                    $stmt->bindParam(3, $animal['raca_id']);
                    $stmt->bindParam(4, $animal['sexo']);
                    $stmt->bindParam(5, $animal['especie']);
                    $stmt->bindParam(6, $animal['data_nascimento']);
                    $stmt->bindParam(7, $animal['data_atualizacao']);
                    $stmt->bindParam(8, $animal['data_falecimento']);

                    $stmt->execute();

                    $sucesso = true;
                    $mensagem = 'Animal atualizado com sucesso';
                } catch(Exception $e){
                    $sucesso = false;
                    $mensagem = 'Erro ao gravar animal';
                    $mensagem = $e->getMessage();
                }
            }

            $retorno = json_encode([
                'sucesso'  => $sucesso,
                'mensagem' => $mensagem,
            ]);

            $response->getBody()->write($retorno);
            return $response;
        });
    });

    $app->group('/racas', function (Group $group) use ($container, $pdo) {

        // Listar
        $group->get('', function (Request $request, Response $response) use ($pdo) {

            $sql = "SELECT * FROM racas";
            $stmt = $pdo->prepare($sql);
            $stmt->execute();
            $results = $stmt->fetchAll();

            $racas = [];
            foreach ($results as $key => $value) {
                $racas[] = $value;
            }

            $racas = json_encode($racas);

            $response->getBody()->write($racas);
            return $response;
        });
    });
};