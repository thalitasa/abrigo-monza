import axios from 'axios';

const urlBaseMarvel = 'http://localhost:8081/';

export default {
    getAnimais: (callback) => {
        axios.get(urlBaseMarvel + 'animais').then((resposta) => {
            if (callback) {
                callback(resposta);
            }
        })
    },
    getRacas: (callback) => {
        axios.get(urlBaseMarvel + 'racas').then((resposta) => {
            if (callback) {
                callback(resposta);
            }
        })
    },
    salvarAnimal: (animal, callback) => {
        axios.post(urlBaseMarvel + 'animais/salvar', JSON.stringify(animal)).then((resposta) => {
            if (callback) {
                callback(resposta);
            }
        })
    },
}