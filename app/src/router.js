import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
Vue.use(Router);
export default new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/animais',
            name: 'animais',
            component: () => import('./views/Animais.vue')
        },
        {
            path: '/adocao',
            name: 'adocao',
            component: () => import('./views/Adocao.vue')
        },
        {
            path: '/animais/form',
            name: 'form-animais',
            component: () => import('./views/AnimaisForm.vue')
        },
    ]
});