-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 08-Set-2019 às 22:46
-- Versão do servidor: 10.4.6-MariaDB
-- versão do PHP: 7.2.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `abrigo-monza`
--

DELIMITER $$
--
-- Procedimentos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `animais_insert` (IN `nome` VARCHAR(255), IN `raca_id` INT, IN `sexo` CHAR(1), IN `especie` CHAR(1), IN `data_nascimento` DATE, IN `data_atualizacao` DATE, IN `data_falecimento` DATE, OUT `status` BOOLEAN, OUT `mensagem` VARCHAR(255))  NO SQL
BEGIN

INSERT INTO animais (nome, raca_id, sexo, especie, data_nascimento, data_atualizacao, data_falecimento)
VALUES (nome, raca_id, sexo, especie, data_nascimento, data_atualizacao, data_falecimento);

SELECT true, 'Animal salvo com sucesso!' INTO status, mensagem;  

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `animais_update` (IN `id` INT, IN `nome` VARCHAR(255), IN `raca_id` INT, IN `sexo` CHAR(1), IN `especie` CHAR(1), IN `data_nascimento` DATE, IN `data_atualizacao` DATE, IN `data_falecimento` DATE)  NO SQL
BEGIN

UPDATE animais a
SET
	nome=nome,
    raca_id=raca_id,
    sexo=sexo,
    especie=especie,
    data_nascimento=data_nascimento,
    data_atualizacao=data_atualizacao,
    data_falecimento=data_falecimento
WHERE a.id=id ;

END$$

--
-- Funções
--
CREATE DEFINER=`root`@`localhost` FUNCTION `idade` (`data_nascimento` DATE) RETURNS INT(11) BEGIN
  DECLARE idade INT;

  SELECT TIMESTAMPDIFF(MONTH, data_nascimento, CURDATE()) INTO idade;

  RETURN idade;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `animais`
--

CREATE TABLE `animais` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `raca_id` int(11) NOT NULL,
  `sexo` char(1) NOT NULL,
  `especie` char(1) NOT NULL,
  `data_nascimento` date NOT NULL,
  `data_atualizacao` date NOT NULL,
  `data_falecimento` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `animais`
--

INSERT INTO `animais` (`id`, `nome`, `raca_id`, `sexo`, `especie`, `data_nascimento`, `data_atualizacao`, `data_falecimento`) VALUES
(1, 'Beca', 1, 'F', '1', '2019-03-17', '2019-09-08', NULL),
(4, 'Igui', 3, 'M', '1', '2017-01-01', '2019-09-08', NULL),
(5, 'Belinha', 5, 'F', '1', '2017-05-20', '2019-09-08', '2019-09-08'),
(9, 'Nilo', 3, 'M', '1', '2007-08-20', '2019-09-08', NULL),
(10, 'Tom ZÃ©', 6, 'M', '2', '2019-01-01', '2019-09-08', NULL),
(21, 'Mel', 7, 'F', '2', '2012-08-10', '2019-09-08', '2015-11-16'),
(22, 'Brad', 4, 'M', '1', '2017-07-16', '2019-09-08', NULL),
(23, 'Cuscuz', 6, 'M', '2', '2015-02-22', '2019-09-08', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `racas`
--

CREATE TABLE `racas` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `situacao` char(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `racas`
--

INSERT INTO `racas` (`id`, `nome`, `situacao`) VALUES
(1, 'Daschund', 'ATI'),
(3, 'Poodle', 'ATI'),
(4, 'Labrador', 'ATI'),
(5, 'Shih Tzu', 'ATI'),
(6, 'SRD', 'ATI'),
(7, 'Persa', 'ATI');

-- --------------------------------------------------------

--
-- Estrutura stand-in para vista `vw_animais`
-- (Veja abaixo para a view atual)
--
CREATE TABLE `vw_animais` (
`id` int(11)
,`nome` varchar(255)
,`raca` varchar(255)
,`raca_id` int(11)
,`situacao` char(3)
,`sexo` char(1)
,`especie` char(1)
,`data_nascimento` date
,`data_atualizacao` date
,`data_falecimento` date
,`idade` int(11)
,`faleceu` int(1)
);

-- --------------------------------------------------------

--
-- Estrutura para vista `vw_animais`
--
DROP TABLE IF EXISTS `vw_animais`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_animais`  AS  select `a`.`id` AS `id`,`a`.`nome` AS `nome`,`r`.`nome` AS `raca`,`r`.`id` AS `raca_id`,`r`.`situacao` AS `situacao`,`a`.`sexo` AS `sexo`,`a`.`especie` AS `especie`,`a`.`data_nascimento` AS `data_nascimento`,`a`.`data_atualizacao` AS `data_atualizacao`,`a`.`data_falecimento` AS `data_falecimento`,`idade`(`a`.`data_nascimento`) AS `idade`,if(`a`.`data_falecimento` is null,0,1) AS `faleceu` from (`animais` `a` join `racas` `r` on(`r`.`id` = `a`.`raca_id`)) ;

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `animais`
--
ALTER TABLE `animais`
  ADD PRIMARY KEY (`id`),
  ADD KEY `raca_id_fk` (`raca_id`);

--
-- Índices para tabela `racas`
--
ALTER TABLE `racas`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `animais`
--
ALTER TABLE `animais`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT de tabela `racas`
--
ALTER TABLE `racas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Restrições para despejos de tabelas
--

--
-- Limitadores para a tabela `animais`
--
ALTER TABLE `animais`
  ADD CONSTRAINT `raca_id_fk` FOREIGN KEY (`raca_id`) REFERENCES `racas` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
