# Instruções para rodar a aplicação

## Banco de dados

  * Crie um banco de dados e importe o dump disponível na pasta `db/`
  * Execute a aplicação do MySQL
  * Abra o arquivo `routes.php`, localizado no diretório `api/app/` do projeto. Coloque as configurações de conexão do banco de dados no local indicado e salve

## API

  * Dentro do diretório `api/`, execute os seguintes comandos:
    * `composer install`
    * `php -S localhost:8081 -t public public/index.php`, para rodar a API

## APP

  * Dentro do diretório `app/`, em outro terminal, execute os seguintes comandos:
    * `yarn install`
    * `yarn serve`, para rodar o APP

## Pronto!

  * Se tudo deu certo, a aplicação pode ser acessada pela url `http://localhost:8080/`